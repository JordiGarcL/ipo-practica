import 'package:flutter/material.dart';
import 'package:ipocoffe/implementation/Game.dart';
import 'package:ipocoffe/implementation/GamesList.dart';
import 'package:ipocoffe/views/booking_list.dart';
import 'package:ipocoffe/views/games_list.dart';
import 'package:ipocoffe/views/preferences.dart';

GamesList gamesList = new GamesList();
List<Game> list = gamesList.list;
///Stateful widget that will create the main app.
class IPOCoffeeBookApp extends StatefulWidget{
  @override
  State createState() => new IPOCoffeeBookAppState();
}

///Main App Screen that sets the title and creates a Tab view that will control everything
class IPOCoffeeBookAppState extends State<IPOCoffeeBookApp> with TickerProviderStateMixin{

  //Controller for the Tab View
  TabController _tabController;
  String appBarTitle = "CoffeBook";

  final formKey = new GlobalKey<FormState>();

  @override
  void initState(){
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(_handleTabSelection);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  _handleTabSelection(){
    setState(() {
      if(_tabController.index == 0) appBarTitle="Lista de juegos";
      else if(_tabController.index == 1) appBarTitle="Tus reservas";
      else if(_tabController.index == 2) appBarTitle="Lista de favoritos";
      else if(_tabController.index == 3) appBarTitle="Ajustes";
    });
  }

  ///Create the Scaffold with all the Tab bars and calls every page
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new CustomScrollView(
        slivers: <Widget>[
          new SliverAppBar(
            backgroundColor: Colors.brown,
            expandedHeight: 100.0,
            floating: true,
            pinned: true,
            //forceElevated: true,
            elevation: 13.0,

            flexibleSpace: new FlexibleSpaceBar(
              background: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    appBarTitle, style: new TextStyle(
                      color: Colors.white,
                      fontSize: 30.0,
                      fontWeight: FontWeight.w700
                  ),
                  ),

                ],
              ),
            ),
            bottom: new TabBar(
              labelColor: Colors.white,
              indicatorColor: Colors.black87,
              isScrollable: false,
              controller: _tabController,
              tabs: <Widget>[
                new Tab(icon: new Icon(Icons.format_list_bulleted),),
                new Tab(icon: new Icon(Icons.access_time),),
                new Tab(icon: new Icon(Icons.favorite_border),),
                new Tab(icon: new Icon(Icons.settings)),
              ],
            ),
          ),
          new SliverFillRemaining(
            child: new TabBarView(
              controller: _tabController,
              children: <Widget>[
                new GameListWidget(list, false),
                new BookingListWidget(list),
                new GameListWidget(list, true),
                new PreferenceWidget(),
                ],
            ),
          ),
        ],
      ),
    );
  }
}