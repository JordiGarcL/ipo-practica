import 'package:random_string/random_string.dart';
import 'dart:math';

class Booking {
  List<String> animals= ["Tiburón","Leon","Gato","Tigre","Perro","Ratón"];
  List<String> colors= ["azul","lila","rosa","verde","amarillo","naranja"];
  String identifier;
  bool user = true;
  String name;
  String duration;
  Booking(bool user, String name, String duration) {
    this.identifier = getID();
    this.user=user;
    this.name = name;
    this.duration=duration;

  }

  String getDuration(int count){
    //var
    String parse = duration;

    int minGameTime = int.parse(parse.split(" ")[0].split("-")[0]);
    int maxGameTime = int.parse(parse.split(" ")[0].split("-")[1]);
    DateTime time = new DateTime.now();
    int currentHour = time.hour;
    int currentMin = time.minute;
    String interval;
    int min;
    int hour;

    //calculate minTime
    min = (minGameTime * count) + currentMin;
    hour = currentHour;
    while ((min >= 60) || (hour >= 24))
    {
      if (min >= 60)
      {
        min=min - 60;
        hour++;
      }
      if (hour >= 24) hour = hour - 24;
    }
    interval = hour.toString().padLeft(2, "0") + ":" + min.toString().padLeft(2, "0") + " - ";
    //calculate maxTime
    min = (maxGameTime * count) + currentMin;
    hour = currentHour;
    while ((min >= 60) || (hour >= 24))
    {
      if (min >= 60)
      {
        min=min-60;
        hour++;
      }
      if (hour >= 24) hour = hour - 24;
    }
    interval = interval + hour.toString().padLeft(2, "0") + ":" + min.toString().padLeft(2, "0");
    return interval;
  }

  String getID(){
    var rng = new Random();
    return(animals[rng.nextInt(5)] + " " + colors[rng.nextInt(5)]);
  }


}
