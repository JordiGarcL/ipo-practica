import 'package:ipocoffe/implementation/Booking.dart';

class Game {

  String photoURI;
  String name;
  String numberOfPlayers;
  String time;
  String description;
  List<Booking> bookings;
  bool isFavourite;

  Game(String photoURI, String name, String numberOfPlayers, String time, String description, List<Booking>bookings,
      bool isFavourite) {
      this.photoURI = photoURI;
      this.name = name;
      this.numberOfPlayers = numberOfPlayers;
      this.time = time;
      this.description = description;
      this.bookings = new List();
      this.isFavourite = isFavourite;
  }


  void bookGame(bool user) {
      Booking book = new Booking(user, name, time);
      bookings.add(book);
  }

  void deleteBook(int index){
    bookings.removeLast();
  }

  Duration getMinDuration() {
    String durationString = time; // Expected format: 'mm-mm min'
    int minDurationInMinutes = int.parse(durationString.split(" ")[0].split("-")[0]);
    Duration minDuration = Duration(minutes: minDurationInMinutes);
    return minDuration;
  }

  Duration getMaxDuration() {
    String durationString = time; // Expected format: 'mm-mm min'
    int maxDurationInMinutes = int.parse(durationString.split(" ")[0].split("-")[1]);
    Duration maxDuration = Duration(minutes: maxDurationInMinutes);
    return maxDuration;
  }

  Duration getMinWaitingTime() {
    Duration minWaitingTime = Duration();
    Duration minDuration = getMinDuration();

    int numberOfBookings = bookings.length;
    for (int i = 0; i < numberOfBookings; i++) {
      minWaitingTime += minDuration;
    }
    return minWaitingTime;
  }

  Duration getMaxWaitingTime() {
    Duration maxWaitingTime = Duration();
    Duration maxDuration = getMaxDuration();

    int numberOfBookings = bookings.length;
    for (int i = 0; i < numberOfBookings; i++) {
      maxWaitingTime += maxDuration;
    }
    return maxWaitingTime;
  }


  String getFormattedWaitingTime() {
    String waitingTimeString = "";

    int minHours = getMinWaitingTime().inHours;
    int minMinutes = (getMinWaitingTime().inMinutes) % 60;

    if (minHours >= 1) waitingTimeString += "${minHours}h ";
    if ((minHours < 1) || (minMinutes != 0)) waitingTimeString += "${minMinutes}m ";

    waitingTimeString += "- ";

    //  Mo me gusta nada este codigo repetido
    int maxHours = getMaxWaitingTime().inHours;
    int maxMinutes = (getMaxWaitingTime().inMinutes) % 60;

    if (maxHours >= 1) waitingTimeString += "${maxHours}h ";
    if ((maxHours < 1) || (maxMinutes != 0)) waitingTimeString += "${maxMinutes}m";

    return waitingTimeString;
  }

  bool hasBookings() {
    return (bookings.isNotEmpty);
  }

  bool hasThisUserQueued() {
    bool thisUserHasABooking = false;
    for (Booking booking in bookings) {
      if (booking.user == true) {
        thisUserHasABooking = true;
        break;
      }
    }
    return thisUserHasABooking;
  }

  int getNumberOfBookings() {
    return bookings.length;
  }

}
