
import 'package:ipocoffe/implementation/Game.dart';

class GamesList {
  List<Game> list;

  GamesList(){
    list = new List();
    getAllGames();
    bookGames();
  }

  void getAllGames() {
    Game dobble = new Game("lib/images/dobble.png", "Dobble", "2 a 8", "15-25 min", "El objetivo principal"
        " del Dobble es encontrar el dibujo que se repite entre dos cartas. Este juego está pensado para que siempre (sin excepción)"
        " haya un dibujo que se repita en la otra carta. La palabra “paso” queda totalmente al margen de las partidas.", new List(), false);
    list.add(dobble);

    Game cuatroenlinea = new Game("lib/images/4enlinea.png", "4 en linea", "2", "15-20 min", "¿Serás el más rápido en"
        " alinear las fichas de tu color? Usa la estrategia y haz fallar a tu contrincante evitando que pueda completar las líneas."
        " Conecta las cuatro fichas de tu color de forma vertical u horizontal y ganarás.", new List(), false);
    list.add(cuatroenlinea);

    Game ajedrez = new Game("lib/images/ajedrez.png", "Ajedrez", "2", "20-60 min", "Se trata de un juego de "
        "estrategia en el que el objetivo es «derrocar» al rey del oponente.", new List(), false);
    list.add(ajedrez);

    Game bang = new Game("lib/images/bang.png", "Bang", "4 a 8", "30-40 min", "La premisa básica de este juego "
        "es matar a tiros a tus rivales. Cada disparo que le demos a un jugador le va restando vidas y sobrevive el último en pie."
        " Toda la dinámica es a través de cartas y por turnos.", new List(), false);
    list.add(bang);

    Game bloquesApilables = new Game("lib/images/bloquesapilables.png", "Bloques apilables", "Más de 2", "15-20 min",
        "Los jugadores, uno por uno, tendrán que quitar un bloque de la torre, y colocarlo en su parte superior.  Lo importante es "
        "quitar bloques de la mitad inferior de la torre y sobre todo, solo utilizar dos dedos de una mano.", new List(), false);
    list.add(bloquesApilables);

    Game carcasonne = new Game("lib/images/carcasonne.png", "Carcasonne", "2 a 5", "30-60 min", "Los jugadores de forma aleatoria "
        "van sacando las fichas del terreno. Las fichas se componen de campos, ciudades, caminos y monasterios. Sobre éstas se colocan las "
        "piezas de los personajes (granjeros, caballeros, monjes o ladrones).", new List(), false);
    list.add(carcasonne);

    Game catan = new Game("lib/images/catan.png", "Catan", "3 a 4", "45-60 min", "El objetivo del juego es construir pueblos, ciudades y caminos sobre un tablero que es distinto cada vez,"
        " mientras se van acumulando varios tipos de cartas. Todos estos elementos proporcionan distintas puntuaciones. ", new List(), false);
    list.add(catan);

    Game domino = new Game("lib/images/domino.png", "Domino", "2 a 4", "35-45 min", "El objetivo del juego"
        " es alcanzar una determinada puntuación previamente prefijada, jugando para ello las manos o rondas que sean precisas. El"
        " jugador que gana una ronda, suma los puntos de las fichas de sus adversarios y/o pareja.", new List(), false);
    list.add(domino);

    Game hundirLaFlota = new Game("lib/images/hundirlaflota.png", "Hundir la flota", "2", "15-20 min", "El "
        "jugador que inicia dice una letra de la A a la J y un número del 1 al 10, el jugador al que ataca tendrá que revisar su"
        " tablero de juego en esas coordenadas, si el barco está completamente lleno de disparos, el grito rival tendrá que ser"
        " de “hundido”. ", new List(), false);
    list.add(hundirLaFlota);

    Game mikado = new Game("lib/images/mikado.png", "Mikado", "2 a 4", "15-25 min", "Para iniciar la partida, un"
        " jugador coge el haz de pailllos en su mano y lo coloca de manera perpendicular a la mesa. A continuación abre la mano"
        " y deja caer los paillos al azar. Gana el jugador con mayor puntuación.", new List(), false);
    list.add(mikado);

    Game monopoly = new Game("lib/images/monopoly.png", "Monopoly", "2 a 8", "40-60 min", "El objetivo"
        " del juego es ganar la mayor cantidad de dinero y bienes mientras logras que tus contrincantes se vayan en banca rota. "
        "Esto se consigue adquiriendo propiedades, construyendo casas y edificios y cobrando renta por tus propiedades.",
        new List(), false);
    list.add(monopoly);

    Game parchisLaOca = new Game("lib/images/parchislaoca.png", "Parchís/La oca", "Más de 2", "15-30 min", "El "
        "objetivo del juego es que cada jugador lleve sus fichas desde la salida hasta la meta intentando, en el camino, comerse a"
        " las demás. El primero en conseguirlo será el ganador.", new List(), false);
    list.add(parchisLaOca);

    Game pasapalabra = new Game("lib/images/pasapalabra.png", "Pasapalabra", "Más de 3", "30-40 min", "El"
        "juego consiste en acertar las veinticinco palabras, cada una de las cuales se corresponde con una letra del rosco."
        " Los aciertos se reflejarán en las letras mediante el color verde, mientras que los fallos se mostrarán en color rojo.", new List(), false);
    list.add(pasapalabra);

    Game queAnimalSoy = new Game("lib/images/queanimalsoy.png", "Que animal soy", "2 a 4", "15-25 min", "En el "
        "centro de la mesa se coloca el cocodrilo, que será la “base” de la pirámide de animales que se va a construir. Ganará el"
        " jugador que se quede antes sin animales.", new List(), false);
    list.add(queAnimalSoy);

    Game rummikub = new Game("lib/images/rummikub.png", "Rummikub", "2 a 4", "30-40 min", "Para poder bajar fichas"
        " a la mesa los jugadores deben tener una o más líneas (combinaciones) que sumen 30 puntos. Los puntos son los que indica el "
        " número de la ficha, siendo el valor del comodín 0.", new List(), false);
    list.add(rummikub);

    Game scrabble = new Game("lib/images/scrabble.png", "Scrabble", "2 a 4", "30-60 min", "Se trata de "
        "crear palabras interconectadas como si de un crucigrama se tratara y emplazarlas de tal manera sobre el tablero que "
        "arrojen la máxima puntuación.", new List(), false);
    list.add(scrabble);

    Game sushiGo = new Game("lib/images/sushigo.png", "Sushi go", "2 a 5", "15-20 min", "Es un juego muy sencillo "
        "en el que mediante una mecánica de draft, jugaremos una serie de turnos en los que los jugadores elegirán una carta de"
        " su mano de forma simultánea, pasando el resto de la mano a otro jugador.", new List(), false);
    list.add(sushiGo);

    Game tabu = new Game("lib/images/tabu.png", "Tabu", "+4", "45-60 min", "Consigue que tu "
        "pareja adivine la palabra de la tarjeta sin usar algunas palabras clave. Todo es cuestión de tener imaginación y mucho "
        "vocabulario.", new List(), false);
    list.add(tabu);

    Game uno = new Game("lib/images/uno.png", "Uno", "2 a 10", "5-35 min", "El objetivo de Uno es deshacerse"
        " de todas las cartas que se “roban” inicialmente, diciendo la palabra Uno cuando queda la última carta en la mano; si el"
        " jugador no dice Uno cuando tiene solo una carta será penalizado con 2 cartas.", new List(), false);
    list.add(uno);
  }
  
  void bookGames(){
    list[0].bookGame(false);
    list[0].bookGame(true);
    list[1].bookGame(true);
  }

  List<Game> getList(){
    return list;
  }
}