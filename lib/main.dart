import 'package:flutter/material.dart';
import 'package:ipocoffe/home.dart';

void main() => runApp(new MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: "CoffeBook",
        home: new IPOCoffeeBookApp(),
        theme: ThemeData.light(),
        routes: <String, WidgetBuilder> {
          // TODO implement route to game info
          //'/item-info': (BuildContext context) => new GameInfo(deck1vs1, "ladder", documentSnapshotLadder, collectionReferenceLadder),
        }
    );
  }
}
