import 'package:flutter/material.dart';
import 'package:ipocoffe/implementation/Game.dart';

class GameViewWidget extends StatefulWidget {

  final Game game;

  GameViewWidget(this.game);

  @override
  State createState() => new GameViewWidgetState(game);
}

class GameViewWidgetState extends State<GameViewWidget> with TickerProviderStateMixin {

  Game game;
  GameViewWidgetState(this.game);

  TabController tabController;
  bool isDescriptionVisible;
  bool isQueuedIntoGame;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
    tabController.addListener(onTabChange);
  }

  // Create the Scaffold with all the Tab bars and calls every page
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: buildAppBar(),
        body: buildBody(),
        backgroundColor: Color(0xFFEBEBEB),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      title: Text("Detalle de juego"),
      backgroundColor: Colors.brown,
      elevation: 0.0,
      toolbarOpacity: 1.0,
    );
  }

  Widget buildBody() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                height: 180.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: ExactAssetImage(game.photoURI),
                    fit: BoxFit.cover,
                    colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.6), BlendMode.srcATop)
                  )
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: buildHeader(),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: buildDescriptionCard(),
              ),
            )
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: buildQueueCard(),
              ),
            ),
          ]
        ),
      ]
    );
  }

  Widget buildHeader() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
      Text(game.name,
          style: TextStyle(
            fontSize: 35.0,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            color: Colors.white,
          ),
        ),
        Container(height: 10.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 100.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.timer,
                      color: Colors.white
                  ),
                  Text(game.time,
                      style: TextStyle(color: Colors.white)
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 100.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.group,
                      color: Colors.white
                  ),
                  Container(width: 4.0),
                  Text(game.numberOfPlayers,
                      style: TextStyle(color: Colors.white)
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Card buildDescriptionCard() {
    return Card(
      elevation: 2.0,
      child: InkWell(
        onTap: () => {},
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Descripción",
                style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold
                ),
              ),
              Container(height: 10.0),
              Text(game.description,
                  style: TextStyle(fontSize: 15.0)
              ),
            ],
          ),
        ),
      ),
    );
  }

  Card buildQueueCard() {
    int numberOfPeopleInQueue = game.getNumberOfBookings();
    String peopleInQueueString;
    if (numberOfPeopleInQueue == 1) peopleInQueueString = "1 persona en la cola";
    else peopleInQueueString = "$numberOfPeopleInQueue personas en la cola";

    String waitingTimeString;
    if (game.hasBookings()) waitingTimeString = game.getFormattedWaitingTime();
    else waitingTimeString = "No hay cola! 😍";

    return Card(
      elevation: 2.0,
      child: InkWell(
        onTap: () => {},
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(peopleInQueueString,
                style: TextStyle(
                fontSize: 30.0,
                ),
              ),
              Text("Tiempo de espera:",
                style: TextStyle(fontSize: 22.0),
              ),
              Text(waitingTimeString,
                style: TextStyle(fontSize: 22.0, color: Colors.black87),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Column(
                  children: buildBookingButtons(game.hasThisUserQueued()),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> buildBookingButtons(bool isQueuedIntoGame) {
    List<Widget> buttonList = List();
    String queueToggleText;
    Color queueToggleColor;

    if (isQueuedIntoGame) {
      queueToggleText = "SALIR DE LA COLA";
      queueToggleColor = Colors.redAccent;
    }
    else {
      queueToggleText = "PONERSE EN LA COLA";
      queueToggleColor = Colors.blueAccent;
    }

    buttonList.add(
      SizedBox(
        width: 260.0,
        child: RaisedButton(
          elevation: 2.0,
          child: Text(queueToggleText,
            style: TextStyle(color: Colors.white, fontSize: 19.0),
          ),
          color: queueToggleColor,
          onPressed: onQueueTogglePressed,
        ),
      ),
    );

    if (isQueuedIntoGame) {
      buttonList.add(
        SizedBox(
          width: 260.0,
          child: RaisedButton(
            elevation: 2.0,
            child: Text("VER ID DE RESERVA",
              style: TextStyle(color: Colors.white, fontSize: 19.0),
            ),
            color: Colors.blueAccent,
            onPressed: onViewBookingPressed,
          ),
        ),
      );
    }
    return buttonList;
  }


  void onViewBookingPressed() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("ID de reserva:"),
          content: Text("\"${game.bookings.last.identifier}\"",
            style: TextStyle(
              fontStyle: FontStyle.italic,
              fontSize: 20.0
            ),
          ),
        );
      }
    );
  }

  void onQueueTogglePressed() {
//    setState(() {
//      isQueuedIntoGame = (!isQueuedIntoGame);
//    });
    if (game.hasThisUserQueued()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("¿Salir de la Cola?"),
              content: Text("Estás a punto de salir de la cola del juego: \"${game.name}.\""),
              actions: [
                FlatButton(
                  child: Text("CANCELAR"),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                FlatButton(
                  child: Text("OK"),
                  onPressed: onUnbookGame,
                )
              ],
            );
          }
      );
    }
    else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("¿Reservar Juego?"),
              content: Text("Estás a punto de ponerte en la cola del juego: \"${game.name}.\""),
              actions: [
                FlatButton(
                  child: Text("CANCELAR"),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                FlatButton(
                  child: Text("OK"),
                  onPressed: onBookGame,
                )
              ],
            );
          }
      );
    }
  }

  void onTabChange() {
    setState(() {
      if (tabController.index == 0) isDescriptionVisible = false;
      else isDescriptionVisible = true;
    });
  }

  void onBookGame() {
    setState(() {
      game.bookGame(true);
    });
    Navigator.of(context).pop();
  }

  void onUnbookGame() {
    setState(() {
      int numberOfBookings = game.bookings.length;
      // Since we expect that the last booking is the one of the current user
      game.deleteBook(numberOfBookings-1);
    });
    Navigator.of(context).pop();
  }
}
