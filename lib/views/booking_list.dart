
import 'package:flutter/material.dart';
import 'package:ipocoffe/implementation/Booking.dart';
import 'package:ipocoffe/implementation/Game.dart';


class BookingListWidget extends StatefulWidget {
  List<Game> gamesList;
  BookingListWidget(this.gamesList);

  @override
  _BookingListWidgetState createState() => _BookingListWidgetState(gamesList);
}

class _BookingListWidgetState extends State<BookingListWidget>{

  List<Game> gamesList;
  _BookingListWidgetState(this.gamesList);
  @override
  Widget build(BuildContext context) {

    return ListView.builder(
      itemCount: widget.gamesList.length,
      itemBuilder: (context, index) {
      if (gamesList[index].bookings.length > 0)
        for (int i=0; i<gamesList[index].bookings.length; i++){
          if (gamesList[index].bookings[i].user){
            return BookingWidget(gamesList[index].bookings[i], i+1, gamesList, index);
          }
        }
      },
    );
  }
}

class BookingWidget extends StatefulWidget{
  Booking book;
  int count;
  List<Game> gamesList;
  int index;
  BookingWidget(this.book, this.count, this.gamesList, this.index);

  @override
  _BookingWidgetState createState() => _BookingWidgetState(book, count, gamesList, index);
}


class _BookingWidgetState extends State<BookingWidget>{

  Booking book;
  int count;
  List<Game> gamesList;
  int index;
  _BookingWidgetState(this.book, this.count, this.gamesList, this.index);

  @override
  Widget build(BuildContext context) {
    return cardWidget();
  }

  Widget cardWidget(){
    return Card(
      elevation: 12.0,
      margin: EdgeInsets.all(10.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: titleWidget(),
            subtitle: subtitleWidget()
            ),
          ButtonTheme.bar( // make buttons use the appropriate styles for cards
            child: ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text('ELIMINAR'),
                  onPressed: () {
                    deleteBook(book, count, gamesList, index);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget titleWidget(){
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(
        "ID: " + book.identifier,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25.0
        ),
      ),
    );
  }

  Widget subtitleWidget() {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(
        "Juego: " + book.name + "\nIntervalo hora de recogida: \n" +
            getDuration(book, count),
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0
        ),
      ),
    );
  }

  String getDuration(Booking book, int count){
    //var
    String parse = book.duration;
    int minGameTime = int.parse(parse.split(" ")[0].split("-")[0]);
    int maxGameTime = int.parse(parse.split(" ")[0].split("-")[1]);
    DateTime time = new DateTime.now();
    int currentHour = time.hour;
    int currentMin = time.minute;
    String interval;
    int min;
    int hour;

    //calculate minTime
    min = (minGameTime * count) + currentMin;
    hour = currentHour;
    while ((min >= 60) || (hour >= 24))
    {
      if (min >= 60)
      {
        min=min - 60;
        hour++;
      }
      if (hour >= 24) hour = hour - 24;
    }
    interval = hour.toString().padLeft(2, "0") + ":" + min.toString().padLeft(2, "0") + " - ";
    //calculate maxTime
    min = (maxGameTime * count) + currentMin;
    hour = currentHour;
    while ((min >= 60) || (hour >= 24))
    {
      if (min >= 60)
      {
        min=min-60;
        hour++;
      }
      if (hour >= 24) hour = hour - 24;
    }
    interval = interval + hour.toString().padLeft(2, "0") + ":" + min.toString().padLeft(2, "0");
    return interval;
  }

  void deleteBook(Booking book, int count, List<Game> gamesList, int index){
    gamesList[index].deleteBook(count-1);
    setState(() {});
  }
}