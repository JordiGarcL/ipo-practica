import 'package:flutter/material.dart';
import 'package:ipocoffe/implementation/Game.dart';
import 'package:ipocoffe/views/GameView.dart';

class GameListWidget extends StatefulWidget {

  List<Game> gamesList;
  bool favouriteView;

  GameListWidget(this.gamesList, this.favouriteView);

  @override
  _GameListWidgetState createState() => _GameListWidgetState(gamesList, favouriteView);
}

class _GameListWidgetState extends State<GameListWidget>{

  List<Game> gamesList;
  bool favouriteView;

  _GameListWidgetState(this.gamesList, this.favouriteView);

  @override
  Widget build(BuildContext context) {
    List<Game> new_list = new List();

    if (!favouriteView)
      new_list = widget.gamesList;
    else{
      for (var i=0; i<widget.gamesList.length;i++){
        if (widget.gamesList[i].isFavourite)
          new_list.add(widget.gamesList[i]);
      }
    }

    return ListView.builder(
      itemCount: new_list.length,
      itemBuilder: (context, index) {
        if (!favouriteView){
          return GameWidget(new_list[index]);
        }
        else if (new_list[index].isFavourite && favouriteView)
          return GameWidget(new_list[index]);

      },
    );
  }
}

class GameWidget extends StatefulWidget{

  Game game;

  GameWidget(this.game);

  @override
  _GameWidgetState createState() => _GameWidgetState(game);
}

class _GameWidgetState extends State<GameWidget>{


  Game game;

  _GameWidgetState(this.game);

  @override
  Widget build(BuildContext context) {
    return cardWidget();
  }

  void _viewGame(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => GameViewWidget(game)),
    );

  }

  Widget cardWidget(){
    return Card(
      elevation: 12.0,
      margin: EdgeInsets.all(10.0),
      child: InkWell(
        onTap: () {_viewGame();},
        child: Container(
          alignment: Alignment.center,
          //height: 125.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: photoWidget(),
                title: titleWidget(),
                subtitle: subtitleWidget(),
                trailing: favouriteWidget(),
              ),
            ],
          ),
        ),
      )
    );
  }

  _changeFavouriteDeckStateAndUpdate() {
    setState(() {
      if (game.isFavourite){
        game.isFavourite = false;
      }
      else game.isFavourite = true;
    });
  }

  Icon _changeIcon() {
    if (game.isFavourite) {
      return new Icon(Icons.favorite);
    }
    else {
      return new Icon(Icons.favorite_border);
    }
  }
  Widget photoWidget(){
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 2.0, right: 2.0),
      padding: EdgeInsets.all(4.0),
      width: 100.0,
      height: 100.0,
      decoration: new BoxDecoration(
        image: DecorationImage(
            image: AssetImage(game.photoURI),
            fit: BoxFit.fill
        ),
      ),
    );
  }

  Widget titleWidget(){
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(
        game.name,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 30.0
        ),
      ),
    );
  }

  Widget subtitleWidget(){
    return Column(
      children: <Widget>[
        queueWidget(),
        playersWidget(),
        timeWidget(),
      ],
    );

  }

  Widget playersWidget(){
    return Row(
      children: <Widget>[
        Icon(Icons.perm_identity),
        Text(
          game.numberOfPlayers,
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 17.0
          ),
        )
      ],
    );
  }

  Widget timeWidget(){
    return Row(
      children: <Widget>[
        Icon(Icons.timer),
        Text(
          game.time.toString(),
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 17.0
          ),
        )
      ],
    );
  }

  Widget queueWidget(){
    String queue = "No hay cola! 😍";
    if (game.hasBookings())
      queue = game.getFormattedWaitingTime() + " de cola";
    return Row(
      children: <Widget>[
        Text(
          queue,
          style: TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 17.0
          ),
        )
      ],
    );
  }

  Widget favouriteWidget(){
    return Container(
      child: IconButton(
          color: Colors.red,
          splashColor: Colors.red,
          highlightColor: Colors.redAccent,
          icon: _changeIcon(),
          onPressed: () => _changeFavouriteDeckStateAndUpdate()
      ),
    );
  }

}

