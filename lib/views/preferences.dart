import 'package:flutter/material.dart';


class PreferenceWidget extends StatefulWidget {

  @override
  _PreferenceWidgetState createState() => _PreferenceWidgetState();
}

class _PreferenceWidgetState extends State<PreferenceWidget>{

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          generalPreferences(),
          Divider(),
          notificationPreferences(),
          Divider(),
        ],
      ),
    );
  }

  Widget generalPreferences(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "General",
          style: TextStyle(
              fontSize: 30.0,
              fontWeight: FontWeight.bold
          ),
        ),
        generalLanguage(),
        changeView(),
      ],
    );
  }

  Widget generalLanguage(){
    return Row(
      children: <Widget>[
        Text(
          "Idioma:                                    ",
          style: TextStyle(
              fontSize: 20.0,
          ),
        ),
        RaisedButton(
          onPressed: () {},
          child: Text("Español"),
        )
      ],
    );
  }

  Widget changeView(){
    return Row(
      children: <Widget>[
        Text(
          "Vista del tiempo:                   ",
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
        RaisedButton(
          onPressed: () {},
          child: Text("Hora"),
        )
      ],
    );
  }

  Widget notificationPreferences(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Notificaciones",
          style: TextStyle(
              fontSize: 30.0,
              fontWeight: FontWeight.bold
          ),
        ),
        notification("Aviso para mis turnos              "),
        notification("Aviso 10 minutos antes           "),
        notification("Aviso para devolver juego        "),
      ],
    );
  }

  Widget notification(String text){
    bool switchValue = false;
    return Row(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
        Switch.adaptive(
            value: switchValue,
            onChanged: (bool value) {
              setState(() {
                switchValue = value;
              });
            }
        ),
      ],
    );
  }
}